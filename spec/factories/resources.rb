FactoryGirl.define do
  factory :resource do
    category nil
    name "MyString"
    description "MyText"
  end
end
