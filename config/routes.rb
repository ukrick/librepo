Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'

  get 'search', to: 'search#index'

  resources :resources do
    get :download, on: :member
  end
  resources :categories

  namespace :admin do
    root to: 'categories#index'
  end

  root to: 'categories#index'
  devise_for :users, controllers: { sessions: 'admin/admin'}
end
