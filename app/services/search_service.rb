class SearchService
  class << self

    def search(query_or_payload, models=[Category, Resource], options={})
      models.map! { |model| classify(model) }
      search_proc = Proc.new { Elasticsearch::Model.search(query_or_payload, models, {size: 100}) }
      invoke_search &search_proc
    end

    private

    def classify(model)
      # trying to rescue string params as well as searching non-indexed models
      ar_model = model.is_a?(Class) ? model : find_by_table(model)
      ar_model.respond_to?(:__elasticsearch__) ? ar_model : raise('This is not an indexed model')
    end

    def invoke_search(&block)
      begin
        yield
      rescue Exception => e
        Rails.logger.error("ELASTICSEARCH ERROR: #{e.message}")
        Rails.logger.error(e.backtrace.join("\n"))
        return []
      end
    end

  end
end