class SearchController < ApplicationController
  def index
    @results = SearchService.search(params[:query]).records.to_a.group_by(&:class)
  end
end
