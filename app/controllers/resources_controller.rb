class ResourcesController < ApplicationController
  before_action :set_resource, only: [:show, :edit, :update, :destroy, :download]

  after_action :verify_authorized, except: [:index, :show, :download]
  after_action :verify_policy_scoped, only: :index

  # GET /resources
  # GET /resources.json
  def index
    @resources = policy_scope(Resource.order :name)

    @authenticated = user_signed_in? && current_user.admin?
  end

  # GET /resources/1
  # GET /resources/1.json
  def show
    @authenticated = user_signed_in?
  end

  # GET /resources/new
  def new
    @resource = Resource.new
    authorize @resource
  end

  # GET /resources/1/edit
  def edit
    authorize @resource
  end

  # POST /resources
  # POST /resources.json
  def create
    @resource = Resource.new(resource_params)
    authorize @resource

    respond_to do |format|
      if @resource.save
        format.html { redirect_to @resource, notice: t(:book_created) }
        format.json { render :show, status: :created, location: @resource }
      else
        format.html { render :new }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /resources/1
  # PATCH/PUT /resources/1.json
  def update
    authorize @resource

    respond_to do |format|
      if @resource.update(resource_params)
        format.html { redirect_to @resource, notice: t(:book_updated) }
        format.json { render :show, status: :ok, location: @resource }
      else
        format.html { render :edit }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /resources/1
  # DELETE /resources/1.json
  def destroy
    @resource.destroy
    authorize @resource

    respond_to do |format|
      format.html { redirect_to categories_url, notice: t(:book_destroyed) }
      format.json { head :no_content }
    end
  end

  def download
    send_file @resource.file.path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resource_params
      params.require(:resource).permit(:category_id, :name, :description, :file, :image)
    end
end
