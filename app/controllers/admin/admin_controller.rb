class Admin::AdminController < Devise::SessionsController
  before_action :store_user_location!, only: :new
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  def new
    super
  end

  protected

  def store_user_location!
    store_location_for(:user, request.referrer)
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || super
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer || super
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :name, :email, :password, :remember_me])
  end
end
