class Resource < ActiveRecord::Base
  belongs_to :category, inverse_of: :resources

  validates :category, presence: true
  validates :name, presence: true, uniqueness: {case_sensitive: false}

  ROOT_PATH = '/srv/ftp/repo'

  has_attached_file :file,
                    path: "#{ROOT_PATH}/:id/:basename.:extension",
                    url: "ftp://repouser:adi@#{ENV['DOMAIN_NAME']}/:id/:basename.:extension"
  validates_attachment_content_type :file, content_type: %w(application/octet-stream application/pdf application/vnd.ms-excel application/vnd.openxmlformats-officedocument.spreadsheetml.sheet application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document image/vnd.djvu)

  has_attached_file :image,
                    styles: {medium: "200x200>", thumb: "100x100>"},
                    convert_options: {
                        all: "-strip",
                        negative: "-negate"
                    },
                    default_url: "http://#{ENV['DOMAIN_NAME']}/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  include Searchable
  include AppAnalyzer

  settings ES_SETTING do
    mappings dynamic: 'true' do
      indexes :id, type: 'integer', index: 'not_analyzed'
      indexes :name, type: 'string', analyzer: 'app_analyzer'
      indexes :description, analyzer: 'snowball'
    end
  end
end
