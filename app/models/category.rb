class Category < ActiveRecord::Base

  has_many :resources, -> { order 'name asc' }, inverse_of: :category, dependent: :destroy

  validates :name, presence: true, uniqueness: {case_sensitive: false}

  include Searchable

  settings do
    mappings do
      indexes :id, type: 'integer', index: 'not_analyzed'
      indexes :name, type: 'keyword'
      indexes :description, analyzer: 'snowball'
    end
  end
end
