class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :registerable, :recoverable,
  devise :database_authenticatable,
         :rememberable, :trackable, :validatable, :authentication_keys => {email: false, login: true}

  octopus_establish_connection(
      adapter: Octopus.config[Rails.env.to_s]['shard_fb']['adapter'],
      database: Octopus.config[Rails.env.to_s]['shard_fb']['database'],
      encoding: Octopus.config[Rails.env.to_s]['shard_fb']['encoding'],
      username: Octopus.config[Rails.env.to_s]['shard_fb']['username'],
      password: Octopus.config[Rails.env.to_s]['shard_fb']['password'],
      host: Octopus.config[Rails.env.to_s]['shard_fb']['host'],
      create: Octopus.config[Rails.env.to_s]['shard_fb']['create'])
  allow_shard :shard_fb

  validates :password, presence: true, confirmation: true, if: :password_required?

  enum role: [:visitor, :admin]

  attr_accessor :login

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if (login = conditions.delete(:login))
      where(conditions.to_h).where(['lower(name) = :value', {value: login.downcase.encode('windows-1251')}]).first
    elsif conditions.has_key?(:name)
      where(conditions.to_h).first
    end
  end
end
