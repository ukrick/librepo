$(document).on 'turbolinks:load', ->
  $('#select_file').click ->
    $('#resource_file').click()

  $('#resource_file').change ->
    $('#select_file').text $(this).val()

  $('#select_image').click ->
    $('#resource_image').click()

  $('#resource_image').change ->
    $('#select_image').text $(this).val()
