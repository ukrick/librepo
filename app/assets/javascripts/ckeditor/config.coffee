$(document).ready ->
  CKEDITOR.editorConfig = (config) ->
    config.language = window.locale
    config.toolbar = [
      ['Source', '-', 'Save', 'NewPage', 'DocProps', 'Preview', 'Print', '-', 'Templates'],
      ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', ' Undo Redo'],
      ['Find', 'Replace', '-', 'SelectAll'],
      ['Link', 'Unlink', 'Anchor'],
      '/',
      ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
      ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'],
      ['Table', 'HorizontalRule', 'SpecialChar'],
      '/',
      ['Format', 'FontSize'],
      ['TextColor', 'BGColor'],
      ['Maximize', 'ShowBlocks'] ]
