json.array!(@resources) do |resource|
  json.extract! resource, :id, :category_id, :name, :description
  json.url resource_url(resource, format: :json)
end
