# This file is used by Rack-based servers to start the application.
#\ puma -b 127.0.0.1 -p 3007
require ::File.expand_path('../config/environment', __FILE__)
run Rails.application