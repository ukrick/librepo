namespace :lib do

  desc 'Create web login'
  task :create_web_login => :environment do
    CreateLoginService.new.call
  end
end
