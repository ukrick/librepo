namespace :lib do

  desc 'Indexing'
  task :indexing => :environment do
    IndexingService.new.call
  end
end
