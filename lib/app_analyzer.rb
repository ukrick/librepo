module AppAnalyzer
  ES_SETTING = {
      index: {
          number_of_shards: 1
      },
      analysis: {
          filter: {
              mynGram: {
                  type: :ngram,
                  min_gram: 4,
                  max_gram: 8
              }
          },
          analyzer: {
              app_analyzer: {
                  type: :custom,
                  tokenizer: :keyword,
                  filter: %w(lowercase asciifolding)
              }
          }
      }
  }
end