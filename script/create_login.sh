#!/bin/sh

# This is an example line for a cron file that create logins every 30 minutes:
# */30 * * * * unix_user /path/to/create_login.sh

/srv/www/docroot/adiftplib/bin/rails runner CreateLoginService.new.call RAILS_ENV=production
