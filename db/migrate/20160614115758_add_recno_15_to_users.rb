class AddRecno15ToUsers < ActiveRecord::Migration

  using(:shard_fb)

  def change
    add_column :users, :recno_15, :integer
  end
end
