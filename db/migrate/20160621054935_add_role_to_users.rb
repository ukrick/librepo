class AddRoleToUsers < ActiveRecord::Migration

  using(:shard_fb)

  def change
    add_column :users, :role, :integer, default: 0
  end
end
