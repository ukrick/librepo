class AddNameToUsers < ActiveRecord::Migration

  using(:shard_fb)

  def change
    add_column :users, :name, :string
  end
end
