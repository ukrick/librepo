class CreateCategories < ActiveRecord::Migration

  using(:shard_my)

  def change
    create_table :categories do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end

    add_index :categories, :name, unique: true
  end
end
