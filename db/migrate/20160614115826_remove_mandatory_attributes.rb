class RemoveMandatoryAttributes < ActiveRecord::Migration

  using(:shard_fb)

  def change
    remove_index :users, column: :email
    change_column_null(:users, :email, true)
  end
end
