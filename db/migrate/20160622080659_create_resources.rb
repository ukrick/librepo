class CreateResources < ActiveRecord::Migration

  using(:shard_my)

  def change
    create_table :resources do |t|
      t.belongs_to :category, index: true, foreign_key: true
      t.string :name
      t.text :description

      t.timestamps null: false
    end

    add_index :resources, :name, unique: true
  end
end
