# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160621054935) do

  create_table "abbreviation_32", primary_key: "recno_32", force: :cascade do |t|
    t.string "id",          limit: 1
    t.string "abbr",        limit: 20
    t.string "description", limit: 90
  end

  add_index "abbreviation_32", ["id", "abbr"], name: "abbr_view"
  add_index "abbreviation_32", ["id", "description"], name: "abbr_main"

  create_table "acquisition_order_92", primary_key: "recno_92", force: :cascade do |t|
    t.integer  "recno93",       limit: 4
    t.integer  "uid",           limit: 4
    t.integer  "u_level",       limit: 2
    t.datetime "bdate"
    t.integer  "recno88",       limit: 4
    t.integer  "recno84",       limit: 4
    t.integer  "need_count",    limit: 2
    t.integer  "has_count",     limit: 2
    t.datetime "last_incoming"
    t.text     "coments"
  end

  add_index "acquisition_order_92", ["recno93"], name: "i_main_92"
  add_index "acquisition_order_92", ["uid", "u_level"], name: "i_makeup_92"

  create_table "apu_75", primary_key: "recno_75", force: :cascade do |t|
    t.string   "text",     limit: 512
    t.string   "comments", limit: 512
    t.datetime "bdate"
    t.datetime "mdate"
  end

  add_index "apu_75", ["recno_75"], name: "pk_apu_75", unique: true

  create_table "apu_index_74", primary_key: "recno_74", force: :cascade do |t|
    t.integer "recno26", limit: 4,  default: 0, null: false
    t.integer "recno76", limit: 4,  default: 0, null: false
    t.string  "ind",     limit: 50
  end

  add_index "apu_index_74", ["recno26"], name: "fk_apu_i_26"
  add_index "apu_index_74", ["recno76"], name: "fk_apu_i_76"
  add_index "apu_index_74", ["recno_74"], name: "pk_apu_index_74", unique: true

  create_table "apu_links_76", primary_key: "recno_76", force: :cascade do |t|
    t.integer "root",    limit: 4
    t.integer "recno75", limit: 4
    t.string  "status",  limit: 10
  end

  add_index "apu_links_76", ["recno75"], name: "fk_apu_l_75_"
  add_index "apu_links_76", ["recno_76"], name: "pk_apu_links_76", unique: true
  add_index "apu_links_76", ["root"], name: "fk_apu_l_75"

  create_table "article_id_4", primary_key: "recno_4", force: :cascade do |t|
    t.integer "recno2",       limit: 4
    t.string  "art_id",       limit: 20
    t.integer "wrong_art_id", limit: 4
    t.string  "f_indicator",  limit: 2
  end

  add_index "article_id_4", ["recno2"], name: "fk_article_id_4_r2"

  create_table "aservice_13", primary_key: "recno_13", force: :cascade do |t|
    t.integer  "uid",      limit: 4
    t.integer  "bid",      limit: 4
    t.string   "bit",      limit: 1,  default: "b"
    t.datetime "vdate"
    t.datetime "udate"
    t.datetime "rdate"
    t.integer  "bin",      limit: 4
    t.integer  "bou",      limit: 4
    t.string   "m_sigla",  limit: 50
    t.integer  "tsession", limit: 4
    t.integer  "rsession", limit: 4
  end

  add_index "aservice_13", ["bid"], name: "as13_r23"
  add_index "aservice_13", ["bin"], name: "fk_aservice_13_bin"
  add_index "aservice_13", ["bou"], name: "fk_aservice_13_bou"
  add_index "aservice_13", ["rsession"], name: "aser_13_rs_15_tses"
  add_index "aservice_13", ["tsession"], name: "aser_13_rs_15_rses"
  add_index "aservice_13", ["uid"], name: "as13_r15"

  create_table "at_titles_31", primary_key: "recno_31", force: :cascade do |t|
    t.integer "recno2",         limit: 4
    t.integer "id",             limit: 4
    t.text    "mix_info"
    t.string  "issn",           limit: 20
    t.integer "dates",          limit: 4
    t.string  "rest_data",      limit: 90
    t.string  "vol_mark",       limit: 20
    t.integer "ref_start_data", limit: 4
    t.integer "part_num",       limit: 4
    t.integer "name_num",       limit: 4
    t.string  "lang_titl",      limit: 10
    t.string  "start_data",     limit: 255
    t.string  "f_indicator",    limit: 2
  end

  add_index "at_titles_31", ["id"], name: "ind_at_titles_31_id"
  add_index "at_titles_31", ["recno2"], name: "fk_at_titles_31_r2"

  create_table "authors_19", primary_key: "recno_19", force: :cascade do |t|
    t.integer "recno2",        limit: 4
    t.integer "id",            limit: 4
    t.string  "start_data",    limit: 512
    t.string  "rest_data",     limit: 20
    t.integer "add_data",      limit: 4
    t.string  "roman_nums",    limit: 10
    t.string  "dates",         limit: 20
    t.string  "adv_rest_data", limit: 50
    t.text    "address"
    t.integer "avt_num",       limit: 4
    t.integer "link_code",     limit: 4
    t.string  "organ_link",    limit: 50
    t.string  "invnum",        limit: 20
    t.string  "f_indicator",   limit: 2
  end

  add_index "authors_19", ["id"], name: "ind_authors_19_id"
  add_index "authors_19", ["recno2"], name: "fk_authors_19_r2"

  create_table "block_disciplines_83", primary_key: "recno_83", force: :cascade do |t|
    t.integer "block_recno26", limit: 4
    t.integer "disc_recno26",  limit: 4
  end

  add_index "block_disciplines_83", ["block_recno26"], name: "db83_br"
  add_index "block_disciplines_83", ["block_recno26"], name: "main_bd_83"
  add_index "block_disciplines_83", ["disc_recno26"], name: "db83_dr"

  create_table "clan_name_21", primary_key: "recno_21", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.integer "id",          limit: 4
    t.string  "start_data",  limit: 50
    t.string  "dates",       limit: 20
    t.integer "avt_num",     limit: 4
    t.integer "link_code",   limit: 4
    t.string  "organ_link",  limit: 50
    t.string  "invnum",      limit: 20
    t.string  "f_indicator", limit: 2
  end

  add_index "clan_name_21", ["id"], name: "ind_clan_name_21_id"
  add_index "clan_name_21", ["recno2"], name: "fk_clan_name_21_r2"

  create_table "class_index_71", primary_key: "recno_71", force: :cascade do |t|
    t.string  "ci",          limit: 10,               null: false
    t.string  "csi",         limit: 10
    t.integer "id",          limit: 4
    t.string  "class_index", limit: 90
    t.integer "tag",         limit: 4
    t.string  "status",      limit: 10, default: "n"
  end

  add_index "class_index_71", ["ci", "csi", "id", "class_index"], name: "main_71"
  add_index "class_index_71", ["ci", "csi", "id", "tag"], name: "main_71_t"

  create_table "class_interpre_73", primary_key: "recno_73", force: :cascade do |t|
    t.string   "ci",            limit: 10,               null: false
    t.string   "csi",           limit: 10
    t.integer  "id",            limit: 4
    t.text     "txt"
    t.string   "interpre_type", limit: 10
    t.integer  "link",          limit: 4
    t.string   "status",        limit: 10, default: "n"
    t.datetime "bdate"
    t.datetime "edate"
  end

  add_index "class_interpre_73", ["ci", "csi", "id", "link", "interpre_type"], name: "main_73"

  create_table "class_links_72", primary_key: "recno_72", force: :cascade do |t|
    t.string  "ci",        limit: 10,               null: false
    t.string  "csi",       limit: 10
    t.integer "id",        limit: 4
    t.integer "forw_link", limit: 4
    t.integer "back_link", limit: 4
    t.integer "weight",    limit: 4,  default: 0
    t.integer "link_type", limit: 4,  default: 0
    t.integer "tag",       limit: 4
    t.string  "sys",       limit: 90
    t.string  "status",    limit: 10, default: "n"
  end

  add_index "class_links_72", ["ci", "csi", "id", "link_type", "back_link", "weight"], name: "main_72_b"
  add_index "class_links_72", ["ci", "csi", "id", "link_type", "forw_link", "weight"], name: "main_72_f"

  create_table "class_txtn_70", primary_key: "recno_70", force: :cascade do |t|
    t.string   "ci",        limit: 10,                null: false
    t.string   "csi",       limit: 10
    t.integer  "id",        limit: 4
    t.string   "txt",       limit: 255
    t.string   "txt_short", limit: 90
    t.string   "txt_sys",   limit: 90
    t.integer  "tag",       limit: 4
    t.string   "status",    limit: 10,  default: "n"
    t.datetime "bdate"
    t.datetime "edate"
  end

  add_index "class_txtn_70", ["ci", "csi", "id"], name: "main_70"

  create_table "codedata_33", primary_key: "recno_33", force: :cascade do |t|
    t.integer "recno2",      limit: 4,  null: false
    t.integer "id",          limit: 4
    t.string  "f_indicator", limit: 2
    t.string  "data",        limit: 36
  end

  add_index "codedata_33", ["id"], name: "ind_codedata_33_id"
  add_index "codedata_33", ["recno2"], name: "fk_codedata_33_r2"

  create_table "cre_source_22", primary_key: "recno_22", force: :cascade do |t|
    t.integer  "recno2",      limit: 4
    t.string   "country",     limit: 2
    t.string   "organ",       limit: 50
    t.datetime "crea_date"
    t.integer  "rules",       limit: 4
    t.string   "sys_code",    limit: 10
    t.string   "f_indicator", limit: 2
  end

  add_index "cre_source_22", ["recno2"], name: "fk_cre_source_22_r2"

  create_table "curriculum_80", primary_key: "recno_80", force: :cascade do |t|
    t.string  "xtype",      limit: 1
    t.string  "txt",        limit: 255
    t.string  "txta",       limit: 50
    t.integer "start_year", limit: 2
    t.integer "edu_year",   limit: 2
    t.string  "spec_kode",  limit: 20
    t.integer "recno79",    limit: 4
    t.integer "time_plan",  limit: 4
  end

  create_table "curriculum_detail_81", primary_key: "recno_81", force: :cascade do |t|
    t.integer "recno80",       limit: 4
    t.integer "block_recno26", limit: 4
    t.integer "disc_recno26",  limit: 4
    t.string  "disc_var",      limit: 1
    t.integer "root",          limit: 4, default: 0
    t.integer "time_start",    limit: 4
    t.integer "time_end",      limit: 4
    t.integer "recno93",       limit: 4
  end

  add_index "curriculum_detail_81", ["block_recno26"], name: "fk_curr_det_br26"
  add_index "curriculum_detail_81", ["block_recno26"], name: "fk_curriculum_detail_br26"
  add_index "curriculum_detail_81", ["disc_recno26"], name: "fk_curr_det_dr26"
  add_index "curriculum_detail_81", ["disc_recno26"], name: "fk_curriculum_detail_dr26"
  add_index "curriculum_detail_81", ["recno80"], name: "cd81_r80"
  add_index "curriculum_detail_81", ["recno93"], name: "fk_curriculum_detail_r93"
  add_index "curriculum_detail_81", ["root"], name: "curriculum_detail_81_idx1"
  add_index "curriculum_detail_81", ["root"], name: "fk_curr_det_root"
  add_index "curriculum_detail_81", ["root"], name: "fk_curriculum_detail_root"

  create_table "des_links_40", primary_key: "recno_40", force: :cascade do |t|
    t.integer "recno2",      limit: 4,   null: false
    t.integer "id",          limit: 4
    t.string  "f_indicator", limit: 2
    t.string  "recno",       limit: 20
    t.string  "author",      limit: 128
    t.string  "public_spr",  limit: 128
    t.string  "pub_date",    limit: 50
    t.string  "public_info", limit: 128
    t.string  "part_num",    limit: 50
    t.string  "name_num",    limit: 255
    t.string  "capacity",    limit: 50
    t.string  "heading",     limit: 512
    t.string  "url",         limit: 255
    t.string  "vol_num",     limit: 50
    t.string  "issn",        limit: 20
    t.string  "isbn",        limit: 20
  end

  add_index "des_links_40", ["id"], name: "ind_des_links_40_id"
  add_index "des_links_40", ["recno2"], name: "fk_des_links_40_r2"

  create_table "descrip_2", primary_key: "recno_2", force: :cascade do |t|
    t.integer  "rootid",               limit: 4
    t.string   "status",               limit: 36
    t.datetime "ver_id"
    t.integer  "ref_0_isbx_3",         limit: 4
    t.integer  "ref_1_isbx_3",         limit: 4
    t.integer  "ref_article_id_4",     limit: 4
    t.integer  "ref_0_reg_num_5",      limit: 4
    t.integer  "ref_1_reg_num_5",      limit: 4
    t.integer  "ref_2_reg_num_5",      limit: 4
    t.integer  "ref_3_reg_num_5",      limit: 4
    t.integer  "ref_lang_6",           limit: 4
    t.integer  "ref_pub_country_7",    limit: 4
    t.integer  "ref_headings_18",      limit: 4
    t.integer  "ref_public_info_8",    limit: 4
    t.integer  "ref_spec_num_9",       limit: 4
    t.integer  "ref_public_spr_10",    limit: 4
    t.integer  "ref_volumes_11",       limit: 4
    t.integer  "ref_series_12",        limit: 4
    t.integer  "ref_0_notes_25",       limit: 4
    t.integer  "ref_1_notes_25",       limit: 4
    t.integer  "ref_2_notes_25",       limit: 4
    t.integer  "ref_5_notes_25",       limit: 4
    t.integer  "ref_9_notes_25",       limit: 4
    t.integer  "ref_11_notes_25",      limit: 4
    t.integer  "ref_13_notes_25",      limit: 4
    t.integer  "ref_16_notes_25",      limit: 4
    t.integer  "ref_20_notes_25",      limit: 4
    t.integer  "ref_21_notes_25",      limit: 4
    t.integer  "ref_26_notes_25",      limit: 4
    t.integer  "ref_27_notes_25",      limit: 4
    t.integer  "ref_30_notes_25",      limit: 4
    t.integer  "ref_33_notes_25",      limit: 4
    t.integer  "ref_0_uni_titles_29",  limit: 4
    t.integer  "ref_1_uni_titles_29",  limit: 4
    t.integer  "ref_uni_title_30",     limit: 4
    t.integer  "ref_10_at_titles_31",  limit: 4
    t.integer  "ref_7_at_titles_31",   limit: 4
    t.integer  "ref_3_at_titles_31",   limit: 4
    t.integer  "ref_6_at_titles_31",   limit: 4
    t.integer  "ref_4_at_titles_31",   limit: 4
    t.integer  "ref_5_at_titles_31",   limit: 4
    t.integer  "ref_1_at_titles_31",   limit: 4
    t.integer  "ref_2_at_titles_31",   limit: 4
    t.integer  "ref_14_at_titles_31",  limit: 4
    t.integer  "ref_9_at_titles_31",   limit: 4
    t.integer  "ref_13_at_titles_31",  limit: 4
    t.integer  "ref_12_at_titles_31",  limit: 4
    t.integer  "ref_0_at_titles_31",   limit: 4
    t.integer  "ref_11_at_titles_31",  limit: 4
    t.integer  "ref_8_at_titles_31",   limit: 4
    t.integer  "ref_0_subjects_13",    limit: 4
    t.integer  "ref_1_subjects_13",    limit: 4
    t.integer  "ref_2_subjects_13",    limit: 4
    t.integer  "ref_5_subjects_13",    limit: 4
    t.integer  "ref_6_subjects_13",    limit: 4
    t.integer  "ref_7_subjects_13",    limit: 4
    t.integer  "ref_8_subjects_13",    limit: 4
    t.integer  "ref_keywords_14",      limit: 4
    t.integer  "ref_place_point_15",   limit: 4
    t.integer  "ref_5_uddk_16",        limit: 4
    t.integer  "ref_6_uddk_16",        limit: 4
    t.integer  "ref_kbk_17",           limit: 4
    t.integer  "ref_othk_24",          limit: 4
    t.integer  "ref_0_authors_19",     limit: 4
    t.integer  "ref_1_authors_19",     limit: 4
    t.integer  "ref_2_authors_19",     limit: 4
    t.integer  "ref_0_organs_20",      limit: 4
    t.integer  "ref_1_organs_20",      limit: 4
    t.integer  "ref_2_organs_20",      limit: 4
    t.integer  "ref_0_clan_name_21",   limit: 4
    t.integer  "ref_1_clan_name_21",   limit: 4
    t.integer  "ref_2_clan_name_21",   limit: 4
    t.integer  "ref_cre_source_22",    limit: 4
    t.integer  "ref_electr_res_27",    limit: 4
    t.integer  "ref_non_convert_28",   limit: 4
    t.integer  "ref_locations_23",     limit: 4
    t.string   "sysdata",              limit: 36
    t.integer  "recno15",              limit: 4
    t.datetime "bdate"
    t.datetime "edate"
    t.string   "press_marks",          limit: 128
    t.string   "author",               limit: 380
    t.string   "heading",              limit: 512
    t.string   "rest_info",            limit: 512
    t.string   "public_year",          limit: 20
    t.string   "volumes",              limit: 50
    t.integer  "ref_0_codedata_33",    limit: 4
    t.integer  "ref_1_codedata_33",    limit: 4
    t.integer  "ref_2_codedata_33",    limit: 4
    t.integer  "ref_3_codedata_33",    limit: 4
    t.integer  "ref_4_codedata_33",    limit: 4
    t.integer  "ref_5_codedata_33",    limit: 4
    t.integer  "ref_6_codedata_33",    limit: 4
    t.integer  "ref_7_codedata_33",    limit: 4
    t.integer  "ref_8_codedata_33",    limit: 4
    t.integer  "ref_subj_category_34", limit: 4
    t.integer  "ref_3_clan_name_21",   limit: 4
    t.integer  "ref_3_notes_25",       limit: 4
    t.integer  "ref_0_des_links_40",   limit: 4
    t.integer  "ref_spec_sved_103",    limit: 4
  end

  add_index "descrip_2", ["rootid"], name: "fk_descrip_2_rootid"
  add_index "descrip_2", ["status"], name: "descrip_2_main"

  create_table "descrip_hash_5", primary_key: "recno_5", force: :cascade do |t|
    t.integer "recno2",    limit: 4,                   null: false
    t.decimal "hash_010a",              precision: 18
    t.decimal "hash_200a",              precision: 18
    t.decimal "hash_700a",              precision: 18
    t.decimal "hash_210d",              precision: 18
    t.string  "hash_iso",  limit: 4096
  end

  add_index "descrip_hash_5", ["hash_010a"], name: "i_hash_5_010a"
  add_index "descrip_hash_5", ["hash_200a"], name: "i_hash_5_200a"
  add_index "descrip_hash_5", ["hash_210d"], name: "i_hash_5_210d"
  add_index "descrip_hash_5", ["hash_700a"], name: "i_hash_5_700a"
  add_index "descrip_hash_5", ["recno2"], name: "dh5_r2"

  create_table "electr_res_27", primary_key: "recno_27", force: :cascade do |t|
    t.integer  "recno2",       limit: 4
    t.integer  "host_name",    limit: 4
    t.integer  "access_code",  limit: 4
    t.integer  "compression",  limit: 4
    t.integer  "path",         limit: 4
    t.datetime "last_date"
    t.integer  "electr_name",  limit: 4
    t.integer  "uni_name",     limit: 4
    t.string   "process_req",  limit: 50
    t.integer  "instruction",  limit: 4
    t.float    "bps",          limit: 8
    t.string   "passwd",       limit: 50
    t.string   "login",        limit: 50
    t.integer  "log_help",     limit: 4
    t.string   "location",     limit: 50
    t.string   "op_system",    limit: 90
    t.integer  "port",         limit: 4
    t.string   "format",       limit: 20
    t.integer  "settings",     limit: 4
    t.integer  "file_size",    limit: 4
    t.integer  "term_emu",     limit: 4
    t.string   "url",          limit: 255
    t.integer  "hours_access", limit: 4
    t.integer  "rec_num",      limit: 4
    t.text     "non_note"
    t.string   "access_meth",  limit: 20
    t.text     "pub_note"
    t.string   "f_indicator",  limit: 2
  end

  add_index "electr_res_27", ["recno2"], name: "fk_electr_res_27_r2"

  create_table "enclosures_26", primary_key: "recno_26", force: :cascade do |t|
    t.integer "recno2",    limit: 4
    t.integer "id",        limit: 4
    t.integer "enclosure", limit: 4
    t.integer "x",         limit: 2
  end

  add_index "enclosures_26", ["enclosure"], name: "e26_en"
  add_index "enclosures_26", ["id"], name: "ind_enclosures_26_id"
  add_index "enclosures_26", ["recno2"], name: "e26_r2"

  create_table "extend_options_11", primary_key: "recno_11", force: :cascade do |t|
    t.datetime "bdate"
    t.string   "status",  limit: 3
    t.datetime "mdate"
    t.string   "title",   limit: 255
    t.text     "options"
  end

  add_index "extend_options_11", ["status", "mdate"], name: "i_main_11"

  create_table "hand_stats_40", primary_key: "recno_40", force: :cascade do |t|
    t.string   "status",      limit: 10, default: "a"
    t.integer  "recno14",     limit: 4
    t.datetime "bdate"
    t.integer  "recno15",     limit: 4
    t.string   "sigla",       limit: 50
    t.integer  "knownarea",   limit: 4
    t.integer  "xcount",      limit: 4
    t.integer  "type_b",      limit: 4
    t.integer  "type_h",      limit: 4
    t.integer  "type_other",  limit: 4
    t.integer  "var_a",       limit: 4
    t.integer  "var_c",       limit: 4
    t.integer  "var_d",       limit: 4
    t.integer  "var_other",   limit: 4
    t.integer  "known_a",     limit: 4
    t.integer  "known_c",     limit: 4
    t.integer  "known_b",     limit: 4
    t.integer  "known_other", limit: 4
    t.integer  "lang_ukr",    limit: 4
    t.integer  "lang_rus",    limit: 4
    t.integer  "lang_other",  limit: 4
    t.string   "s_status",    limit: 10
    t.integer  "s_education", limit: 4
    t.integer  "s_category",  limit: 4
    t.datetime "s_byear"
    t.integer  "type_e",      limit: 4
    t.integer  "type_f",      limit: 4
    t.integer  "type_m",      limit: 4
    t.integer  "var_b",       limit: 4
    t.integer  "var_r",       limit: 4
    t.integer  "var_h",       limit: 4
    t.integer  "var_n",       limit: 4
    t.integer  "known_f",     limit: 4
    t.integer  "known_k",     limit: 4
    t.integer  "known_t",     limit: 4
    t.integer  "known_v",     limit: 4
    t.integer  "known_u",     limit: 4
    t.integer  "known_p",     limit: 4
    t.integer  "lang_eng",    limit: 4
    t.integer  "lang_ger",    limit: 4
    t.integer  "lang_fre",    limit: 4
    t.integer  "recno55",     limit: 4
    t.integer  "res_id",      limit: 4
  end

  add_index "hand_stats_40", ["bdate"], name: "hand_stats_40_date"
  add_index "hand_stats_40", ["recno14"], name: "hs40_r14"
  add_index "hand_stats_40", ["recno15"], name: "hs40_r15"
  add_index "hand_stats_40", ["recno55"], name: "hs40_r55"
  add_index "hand_stats_40", ["res_id"], name: "fk_hs_40_electr_res"
  add_index "hand_stats_40", ["sigla"], name: "hand_stats_40_sigla"

  create_table "headings_18", primary_key: "recno_18", force: :cascade do |t|
    t.integer "recno2",          limit: 4
    t.string  "start_data",      limit: 2048
    t.string  "rest_data",       limit: 50
    t.integer "parallel_data",   limit: 4
    t.integer "ref_start_data",  limit: 4
    t.integer "first_onus",      limit: 4
    t.integer "rest_first_onus", limit: 4
    t.integer "part_num",        limit: 4
    t.integer "name_num",        limit: 4
    t.integer "vol_mark",        limit: 4
    t.string  "lang_par_titl",   limit: 20
    t.string  "organ_link",      limit: 50
    t.string  "f_indicator",     limit: 2
  end

  add_index "headings_18", ["recno2"], name: "fk_headings_18_r2"

  create_table "i_books_53", primary_key: "recno_53", force: :cascade do |t|
    t.integer  "recno52",    limit: 4
    t.string   "b_type",     limit: 1,  default: "i"
    t.datetime "b_date"
    t.string   "b_name",     limit: 50, default: ""
    t.integer  "b_num",      limit: 4,  default: 1
    t.integer  "page_count", limit: 4,  default: 100
    t.integer  "num_inpage", limit: 4,  default: 25
    t.integer  "start_num",  limit: 4,  default: 0
  end

  add_index "i_books_53", ["recno52"], name: "ib53_r52"

  create_table "images_54", id: false, force: :cascade do |t|
    t.integer "id",  limit: 4
    t.binary  "pic"
  end

  create_table "invtypes_52", primary_key: "recno_52", force: :cascade do |t|
    t.string  "prefix", limit: 3
    t.string  "name",   limit: 50
    t.integer "cvalue", limit: 4,  default: 0
    t.string  "itype",  limit: 3,  default: "i"
  end

  create_table "isbx_3", primary_key: "recno_3", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.integer "id",          limit: 4
    t.string  "isxn",        limit: 20
    t.string  "rest_data",   limit: 50
    t.integer "cond_price",  limit: 4
    t.integer "abate_issn",  limit: 4
    t.integer "wrong_isxn",  limit: 4
    t.integer "drawn",       limit: 4
    t.string  "f_indicator", limit: 2
  end

  add_index "isbx_3", ["id"], name: "ind_isbx_3_id"
  add_index "isbx_3", ["recno2"], name: "fk_isbx_3_r2"

  create_table "kbk_17", primary_key: "recno_17", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.string  "class_index", limit: 50
    t.string  "book_num",    limit: 20
    t.string  "f_indicator", limit: 2
  end

  add_index "kbk_17", ["recno2"], name: "fk_kbk_17_r2"

  create_table "keywords_14", primary_key: "recno_14", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.string  "keyword",     limit: 90
    t.string  "f_indicator", limit: 2
  end

  add_index "keywords_14", ["keyword"], name: "keywords_14_idx1"
  add_index "keywords_14", ["recno2"], name: "fk_keywords_14_r2"

  create_table "known_disc_94", id: false, force: :cascade do |t|
    t.integer "recno26", limit: 4
    t.integer "recno2",  limit: 4
  end

  add_index "known_disc_94", ["recno2"], name: "fk_kn_disc_2"
  add_index "known_disc_94", ["recno26"], name: "fk_kn_disc_26"

  create_table "known_supplying_79", id: false, force: :cascade do |t|
    t.integer  "recno81", limit: 4
    t.integer  "state",   limit: 2
    t.integer  "recno2",  limit: 4
    t.integer  "recno93", limit: 4
    t.datetime "bdate"
  end

  add_index "known_supplying_79", ["recno2"], name: "fk_kn_supp_2"
  add_index "known_supplying_79", ["recno81"], name: "fk_kn_supp_81"
  add_index "known_supplying_79", ["recno93"], name: "fk_for_key_r93"

  create_table "known_types_93", primary_key: "recno_93", force: :cascade do |t|
    t.string "title", limit: 50
  end

  create_table "lang_6", primary_key: "recno_6", force: :cascade do |t|
    t.integer "recno2",          limit: 4
    t.string  "language",        limit: 20
    t.string  "lang_midl_trans", limit: 20
    t.string  "lang_origin",     limit: 20
    t.string  "sum_lang",        limit: 20
    t.string  "title_lang",      limit: 20
    t.string  "title_list_lang", limit: 20
    t.string  "main_title_lang", limit: 10
    t.string  "acc_mat_lang",    limit: 20
    t.string  "f_indicator",     limit: 2
  end

  add_index "lang_6", ["recno2"], name: "fk_lang_6_r2"

  create_table "list_doc_10", primary_key: "recno_10", force: :cascade do |t|
    t.string   "status",      limit: 10
    t.integer  "sender",      limit: 4
    t.string   "name",        limit: 90
    t.string   "file_name",   limit: 255
    t.string   "file_type",   limit: 10
    t.float    "file_size",   limit: 8
    t.string   "file_about",  limit: 255
    t.datetime "bdate"
    t.integer  "count_rec",   limit: 4
    t.integer  "doc_content", limit: 4
    t.integer  "converter",   limit: 4
    t.datetime "cdate"
    t.string   "comments",    limit: 255
  end

  add_index "list_doc_10", ["status", "sender"], name: "i_main_10"

  create_table "list_user_9", primary_key: "recno_9", force: :cascade do |t|
    t.string   "status",        limit: 10
    t.datetime "bdate"
    t.string   "organ_title",   limit: 255
    t.string   "organ_address", limit: 255
    t.string   "organ_phone",   limit: 20
    t.string   "organ_email",   limit: 50
    t.integer  "organ_city",    limit: 4
    t.string   "user_name",     limit: 90
    t.string   "user_nick",     limit: 20
    t.string   "user_pass",     limit: 50
    t.integer  "trust",         limit: 4
  end

  create_table "locations_23", primary_key: "recno_23", force: :cascade do |t|
    t.integer  "recno2",         limit: 4
    t.string   "location",       limit: 50
    t.string   "sigla",          limit: 50
    t.string   "shelv",          limit: 20
    t.integer  "price",          limit: 4
    t.string   "press_mark",     limit: 50
    t.string   "authr_sign",     limit: 10
    t.string   "keep_code",      limit: 50
    t.string   "pre_keep_code",  limit: 20
    t.string   "shelv_form",     limit: 20
    t.string   "suff_keep_code", limit: 20
    t.float    "invnum",         limit: 8
    t.string   "num",            limit: 20
    t.text     "non_note"
    t.text     "pub_note"
    t.string   "ls",             limit: 20, default: "n"
    t.string   "t_sigla",        limit: 50
    t.datetime "bdate"
    t.integer  "recno90",        limit: 4
    t.integer  "recno41",        limit: 4
    t.string   "f_indicator",    limit: 2
    t.integer  "recno22",        limit: 4
    t.integer  "recno110",       limit: 4
    t.integer  "recno84",        limit: 4
    t.date     "inventory_date"
    t.integer  "recno45",        limit: 4
  end

  add_index "locations_23", ["invnum", "ls"], name: "abon_1"
  add_index "locations_23", ["ls", "sigla", "t_sigla"], name: "abon_2"
  add_index "locations_23", ["recno110"], name: "l23_r110"
  add_index "locations_23", ["recno2"], name: "fk_locations_23_r2"
  add_index "locations_23", ["recno22"], name: "l23_r22"
  add_index "locations_23", ["recno41"], name: "l23_r41"
  add_index "locations_23", ["recno45"], name: "fk_23_stock_45"
  add_index "locations_23", ["recno84"], name: "l23_r84"
  add_index "locations_23", ["recno90", "recno2"], name: "i_locations_wb"
  add_index "locations_23", ["recno90"], name: "l23_r90"

  create_table "mass_actions_57", primary_key: "recno_57", force: :cascade do |t|
    t.integer  "root",          limit: 4,   default: 0
    t.datetime "bdate"
    t.datetime "mdate"
    t.integer  "act_type",      limit: 4
    t.string   "act_title",     limit: 255
    t.integer  "books_count",   limit: 4
    t.integer  "readers_count", limit: 4
    t.integer  "order_count",   limit: 4
    t.string   "sigla",         limit: 50
    t.text     "co_siglas"
    t.string   "status",        limit: 10
  end

  add_index "mass_actions_57", ["root"], name: "ma57_root"
  add_index "mass_actions_57", ["status", "root"], name: "main_57"

  create_table "memos_4", primary_key: "recno_4", force: :cascade do |t|
    t.integer "id",   limit: 4
    t.text    "memo"
  end

  create_table "non_convert_28", primary_key: "recno_28", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.string  "label",       limit: 10
    t.text    "data"
    t.string  "code",        limit: 20
    t.string  "f_indicator", limit: 2
  end

  add_index "non_convert_28", ["recno2"], name: "fk_non_convert_28_r2"

  create_table "notes_25", primary_key: "recno_25", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.integer "id",          limit: 4
    t.string  "organ_link",  limit: 90
    t.string  "invnum",      limit: 20
    t.string  "serial_num",  limit: 20
    t.string  "dates",       limit: 20
    t.text    "note"
    t.string  "f_indicator", limit: 2
  end

  add_index "notes_25", ["id"], name: "ind_notes_25_id"
  add_index "notes_25", ["recno2"], name: "fk_notes_25_r2"

  create_table "options", primary_key: "recno", force: :cascade do |t|
    t.integer "currentin", limit: 4
    t.string  "title",     limit: 50
    t.text    "memo"
  end

  create_table "order_book_27", primary_key: "recno_27", force: :cascade do |t|
    t.string   "status",    limit: 10, default: "n"
    t.integer  "order_id",  limit: 4
    t.integer  "recno15",   limit: 4
    t.datetime "bdate"
    t.datetime "mdate"
    t.integer  "recno2",    limit: 4
    t.date     "on_date"
    t.integer  "recno14",   limit: 4
    t.datetime "take_date"
    t.integer  "recno41",   limit: 4
    t.text     "note"
    t.integer  "recno16",   limit: 4
    t.integer  "shelf",     limit: 4
    t.integer  "recno13",   limit: 4
  end

  add_index "order_book_27", ["recno13"], name: "ob27_r13"
  add_index "order_book_27", ["recno14"], name: "ob27_r14"
  add_index "order_book_27", ["recno15"], name: "ob27_r15"
  add_index "order_book_27", ["recno16"], name: "ob27_r16"
  add_index "order_book_27", ["recno2"], name: "ob27_r2"
  add_index "order_book_27", ["recno41"], name: "ob27_r41"

  create_table "org_tree_41", primary_key: "recno_41", force: :cascade do |t|
    t.integer  "root",       limit: 4
    t.string   "status",     limit: 20
    t.integer  "ind",        limit: 4
    t.string   "txt",        limit: 255
    t.string   "txta",       limit: 50
    t.integer  "peo_count",  limit: 4
    t.integer  "cheef",      limit: 4
    t.integer  "resp",       limit: 4
    t.string   "sid",        limit: 50
    t.integer  "start_year", limit: 4
    t.integer  "address",    limit: 4
    t.datetime "bdate"
    t.datetime "mdate"
    t.string   "subid",      limit: 255
    t.integer  "tag",        limit: 4
    t.integer  "tag1",       limit: 4
  end

  add_index "org_tree_41", ["root", "ind"], name: "i_org_tree_main"
  add_index "org_tree_41", ["root"], name: "fk_41_root"
  add_index "org_tree_41", ["status"], name: "org_tree_41_status"

  create_table "organs_20", primary_key: "recno_20", force: :cascade do |t|
    t.integer "recno2",       limit: 4
    t.integer "id",           limit: 4
    t.string  "start_data",   limit: 90
    t.integer "organ",        limit: 4
    t.integer "token_id",     limit: 4
    t.integer "organ_num",    limit: 4
    t.string  "inst_locat",   limit: 50
    t.string  "inst_date",    limit: 50
    t.string  "back_data",    limit: 50
    t.integer "rest_bs_data", limit: 4
    t.text    "address"
    t.integer "avt_num",      limit: 4
    t.integer "link_code",    limit: 4
    t.string  "organ_link",   limit: 50
    t.string  "invnum",       limit: 20
    t.string  "f_indicator",  limit: 2
  end

  add_index "organs_20", ["id"], name: "ind_organs_20_id"
  add_index "organs_20", ["recno2"], name: "fk_organs_20_r2"

  create_table "othk_24", primary_key: "recno_24", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.string  "class_index", limit: 50
    t.string  "book_num",    limit: 20
    t.string  "brack_class", limit: 20
    t.string  "sys_code",    limit: 10
    t.string  "f_indicator", limit: 2
  end

  add_index "othk_24", ["recno2"], name: "fk_othk_24_r2"

  create_table "pdclassifier_89", primary_key: "recno_89", force: :cascade do |t|
    t.integer "recno88", limit: 4
    t.integer "id",      limit: 4
    t.string  "xindex",  limit: 50
    t.string  "txt",     limit: 255
  end

  add_index "pdclassifier_89", ["recno88"], name: "p89_r88"

  create_table "pdescripts_88", primary_key: "recno_88", force: :cascade do |t|
    t.string   "status",          limit: 36
    t.string   "consys",          limit: 10
    t.text     "maindescription"
    t.integer  "publisher",       limit: 4
    t.float    "cslevel1",        limit: 8
    t.string   "cslevel2",        limit: 50
    t.datetime "edate"
    t.string   "isbn",            limit: 50
    t.string   "price",           limit: 20
    t.string   "author",          limit: 128
    t.string   "heading",         limit: 512
    t.string   "pub_location",    limit: 20
    t.string   "pub_name",        limit: 90
    t.string   "public_year",     limit: 20
    t.string   "volumes",         limit: 50
  end

  create_table "pdlink_87", primary_key: "recno_87", force: :cascade do |t|
    t.integer "recno86", limit: 4
    t.integer "recno88", limit: 4
    t.string  "reg_num", limit: 20
    t.integer "price",   limit: 4
    t.string  "ptype",   limit: 10
    t.string  "status",  limit: 10
  end

  add_index "pdlink_87", ["recno88"], name: "pd87_r88"

  create_table "permitlists_22", primary_key: "recno_22", force: :cascade do |t|
    t.string   "status",  limit: 3,   default: "n"
    t.datetime "bdate"
    t.datetime "mdate"
    t.integer  "num",     limit: 4
    t.integer  "recno90", limit: 4
    t.string   "title",   limit: 255
    t.integer  "recno15", limit: 4
    t.integer  "recno11", limit: 4
  end

  create_table "place_point_15", primary_key: "recno_15", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.string  "country",     limit: 50
    t.string  "state",       limit: 50
    t.integer "depart",      limit: 4
    t.string  "city",        limit: 20
    t.integer "avt_num",     limit: 4
    t.string  "f_indicator", limit: 2
  end

  add_index "place_point_15", ["recno2"], name: "fk_place_point_15_r2"

  create_table "price_index_23", primary_key: "recno_23", force: :cascade do |t|
    t.integer  "recno52", limit: 4
    t.datetime "bdate"
    t.datetime "mdate"
    t.float    "findex",  limit: 8
    t.float    "tindex",  limit: 8
    t.string   "formula", limit: 50, null: false
  end

  add_index "price_index_23", ["recno52"], name: "pi23_r52"

  create_table "price_lists_86", primary_key: "recno_86", force: :cascade do |t|
    t.integer  "recno84",   limit: 4
    t.string   "id",        limit: 10
    t.string   "txt",       limit: 90
    t.datetime "comedate"
    t.datetime "startdate"
    t.datetime "enddate"
    t.string   "status",    limit: 3,  default: "n"
  end

  add_index "price_lists_86", ["recno84"], name: "pl86_r84"

  create_table "pub_country_7", primary_key: "recno_7", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.string  "pub_country", limit: 2
    t.string  "pub_place",   limit: 10
    t.string  "f_indicator", limit: 2
  end

  add_index "pub_country_7", ["recno2"], name: "fk_pub_country_7_r2"

  create_table "public_info_8", primary_key: "recno_8", force: :cascade do |t|
    t.integer "recno2",           limit: 4
    t.string  "pub_inf",          limit: 50
    t.integer "rest_pub_inf",     limit: 4
    t.integer "parallel_pub_inf", limit: 4
    t.integer "pub_onus",         limit: 4
    t.integer "rest_pub_onus",    limit: 4
    t.string  "f_indicator",      limit: 2
  end

  add_index "public_info_8", ["recno2"], name: "fk_public_info_8_r2"

  create_table "public_spr_10", primary_key: "recno_10", force: :cascade do |t|
    t.integer "recno2",       limit: 4
    t.string  "pub_location", limit: 20
    t.text    "address"
    t.string  "pub_name",     limit: 90
    t.string  "pub_date",     limit: 10
    t.string  "crea_locat",   limit: 50
    t.text    "crea_address"
    t.string  "crea_name",    limit: 90
    t.string  "make_date",    limit: 10
    t.string  "f_indicator",  limit: 2
  end

  add_index "public_spr_10", ["recno2"], name: "fk_public_spr_10_r2"

  create_table "r_sys_sint", primary_key: "recno", force: :cascade do |t|
    t.integer "ltable", limit: 4
    t.integer "lfield", limit: 4
    t.integer "link",   limit: 4
    t.integer "data",   limit: 4
    t.integer "recno2", limit: 4
  end

  add_index "r_sys_sint", ["ltable", "lfield", "link"], name: "r_sys_sint_main"
  add_index "r_sys_sint", ["recno2"], name: "fk_sys_sint_r2"

  create_table "r_sys_str10", primary_key: "recno", force: :cascade do |t|
    t.integer "ltable", limit: 4
    t.integer "lfield", limit: 4
    t.integer "link",   limit: 4
    t.string  "data",   limit: 10
    t.integer "recno2", limit: 4
  end

  add_index "r_sys_str10", ["ltable", "lfield", "link"], name: "r_sys_str10_main"
  add_index "r_sys_str10", ["recno2"], name: "fk_sys_str10_r2"

  create_table "r_sys_str20", primary_key: "recno", force: :cascade do |t|
    t.integer "ltable", limit: 4
    t.integer "lfield", limit: 4
    t.integer "link",   limit: 4
    t.string  "data",   limit: 20
    t.integer "recno2", limit: 4
  end

  add_index "r_sys_str20", ["ltable", "lfield", "link"], name: "r_sys_str20_main"
  add_index "r_sys_str20", ["recno2"], name: "fk_sys_str20_r2"

  create_table "r_sys_str255", primary_key: "recno", force: :cascade do |t|
    t.integer "ltable", limit: 4
    t.integer "lfield", limit: 4
    t.integer "link",   limit: 4
    t.string  "data",   limit: 255
    t.integer "recno2", limit: 4
  end

  add_index "r_sys_str255", ["ltable", "lfield", "link"], name: "r_sys_str255_main"
  add_index "r_sys_str255", ["recno2"], name: "fk_sys_str255_r2"

  create_table "r_sys_str50", primary_key: "recno", force: :cascade do |t|
    t.integer "ltable", limit: 4
    t.integer "lfield", limit: 4
    t.integer "link",   limit: 4
    t.string  "data",   limit: 50
    t.integer "recno2", limit: 4
  end

  add_index "r_sys_str50", ["ltable", "lfield", "link"], name: "r_sys_str50_main"
  add_index "r_sys_str50", ["recno2"], name: "fk_sys_str50_r2"

  create_table "r_sys_str90", primary_key: "recno", force: :cascade do |t|
    t.integer "ltable", limit: 4
    t.integer "lfield", limit: 4
    t.integer "link",   limit: 4
    t.string  "data",   limit: 90
    t.integer "recno2", limit: 4
  end

  add_index "r_sys_str90", ["ltable", "lfield", "link"], name: "r_sys_str90_main"
  add_index "r_sys_str90", ["recno2"], name: "fk_sys_str90_r2"

  create_table "raddr_17", primary_key: "recno_17", force: :cascade do |t|
    t.integer  "recno15",    limit: 4
    t.string   "ra_stat",    limit: 1
    t.string   "post_index", limit: 10
    t.text     "addr_txt"
    t.string   "home_phone", limit: 20
    t.text     "notes"
    t.datetime "bdate"
    t.datetime "mdate"
  end

  add_index "raddr_17", ["recno15"], name: "main_rap_17"
  add_index "raddr_17", ["recno15"], name: "r17_r15"

  create_table "reader_ot_42", primary_key: "recno_42", force: :cascade do |t|
    t.integer  "recno15", limit: 4
    t.integer  "recno41", limit: 4
    t.string   "card_id", limit: 20
    t.datetime "bdate"
    t.datetime "mdate"
  end

  add_index "reader_ot_42", ["bdate"], name: "reader_ot_42_date"
  add_index "reader_ot_42", ["recno15"], name: "fk_rot42_r15"
  add_index "reader_ot_42", ["recno41"], name: "fk_rot42_r41"
  add_index "reader_ot_42", ["recno_42"], name: "pk_reader_ot_42", unique: true

  create_table "reader_sessions_55", primary_key: "recno_55", force: :cascade do |t|
    t.integer  "recno15", limit: 4
    t.datetime "started"
    t.datetime "ended"
    t.integer  "recno14", limit: 4
  end

  add_index "reader_sessions_55", ["recno14"], name: "rs55_r14"
  add_index "reader_sessions_55", ["recno15"], name: "i_reader_sessions_main"
  add_index "reader_sessions_55", ["recno15"], name: "rs55_r15"
  add_index "reader_sessions_55", ["started"], name: "reader_sessions_55_started"

  create_table "readers_15", primary_key: "recno_15", force: :cascade do |t|
    t.string   "status",    limit: 10
    t.string   "first",     limit: 50
    t.string   "midle",     limit: 50
    t.string   "last",      limit: 50
    t.datetime "byear"
    t.string   "sex",       limit: 1
    t.float    "id_code",   limit: 8
    t.integer  "card_id",   limit: 4
    t.integer  "doc_type",  limit: 4
    t.string   "docum",     limit: 20
    t.integer  "education", limit: 4
    t.integer  "category",  limit: 4
    t.integer  "recno16",   limit: 4
    t.integer  "recno17",   limit: 4
    t.string   "phone",     limit: 20
    t.string   "email",     limit: 50
    t.datetime "bdate"
    t.datetime "mdate"
    t.datetime "redate"
    t.binary   "image"
    t.text     "note"
    t.string   "fio",       limit: 152
    t.string   "barcode",   limit: 50
    t.integer  "out_id",    limit: 4
    t.integer  "pin",       limit: 2
  end

  add_index "readers_15", ["bdate"], name: "readers_15_bdate"
  add_index "readers_15", ["card_id"], name: "readers_main"
  add_index "readers_15", ["card_id"], name: "unq1_readers_15", unique: true
  add_index "readers_15", ["category"], name: "fc_r_15_category"
  add_index "readers_15", ["doc_type"], name: "fc_r_15_doc_type"
  add_index "readers_15", ["education"], name: "fc_r_15_edu"
  add_index "readers_15", ["recno16"], name: "i_deps_16"
  add_index "readers_15", ["redate"], name: "readers_15_redate"

  create_table "reg_num_5", primary_key: "recno_5", force: :cascade do |t|
    t.integer "recno2",       limit: 4
    t.integer "id",           limit: 4
    t.string  "num_reg",      limit: 10
    t.string  "country_code", limit: 2
    t.string  "number",       limit: 20
    t.string  "rec_id",       limit: 20
    t.integer "wrong_num",    limit: 4
    t.string  "f_indicator",  limit: 2
  end

  add_index "reg_num_5", ["id"], name: "ind_reg_num_5_id"
  add_index "reg_num_5", ["recno2"], name: "fk_reg_num_5_r2"

  create_table "reregisters_58", primary_key: "recno_58", force: :cascade do |t|
    t.integer  "recno15", limit: 4
    t.datetime "bredate"
  end

  add_index "reregisters_58", ["bredate"], name: "reregisters_58_date"
  add_index "reregisters_58", ["recno15"], name: "main_58"
  add_index "reregisters_58", ["recno15"], name: "r58_r15"

  create_table "series_12", primary_key: "recno_12", force: :cascade do |t|
    t.integer "recno2",         limit: 4
    t.string  "start_data",     limit: 255
    t.integer "parallel_data",  limit: 4
    t.integer "ref_start_data", limit: 4
    t.integer "onus",           limit: 4
    t.integer "part_num",       limit: 4
    t.integer "name_num",       limit: 4
    t.integer "vol_mark",       limit: 4
    t.integer "issn",           limit: 4
    t.string  "lang_par_titl",  limit: 20
    t.string  "f_indicator",    limit: 2
  end

  add_index "series_12", ["recno2"], name: "fk_series_12_r2"

  create_table "sessions_16", primary_key: "recno_16", force: :cascade do |t|
    t.integer  "uid",   limit: 4
    t.datetime "bdate"
    t.datetime "mdate"
    t.string   "utype", limit: 1, default: "u"
  end

  add_index "sessions_16", ["uid"], name: "sessions_16_idx1"

  create_table "spec_num_9", primary_key: "recno_9", force: :cascade do |t|
    t.integer "recno2",       limit: 4
    t.string  "date_vol_num", limit: 50
    t.string  "sour_inf_num", limit: 50
    t.string  "f_indicator",  limit: 2
  end

  add_index "spec_num_9", ["recno2"], name: "fk_spec_num_9_r2"

  create_table "spec_sved_103", primary_key: "recno_103", force: :cascade do |t|
    t.integer "recno2",      limit: 4,   null: false
    t.integer "id",          limit: 4
    t.string  "f_indicator", limit: 2
    t.string  "sved",        limit: 255
  end

  add_index "spec_sved_103", ["recno2"], name: "spec_sved_103_main"

  create_table "sqls", primary_key: "machine", force: :cascade do |t|
    t.text "sqlt"
  end

  create_table "ss_periods_102", primary_key: "recno_102", force: :cascade do |t|
    t.string   "status",     limit: 3,  default: "n"
    t.integer  "p_year",     limit: 4
    t.string   "p_name",     limit: 90
    t.integer  "recno84",    limit: 4
    t.datetime "b_date"
    t.datetime "m_date"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "recno41",    limit: 4
  end

  add_index "ss_periods_102", ["p_year"], name: "i_ss_periods_main"
  add_index "ss_periods_102", ["recno41"], name: "fk_ssp_102_r41"
  add_index "ss_periods_102", ["recno84"], name: "s102_r84"

  create_table "statistic_d", primary_key: "sigla", force: :cascade do |t|
    t.date    "dated",                 null: false
    t.integer "type_b",      limit: 4
    t.integer "type_e",      limit: 4
    t.integer "type_f",      limit: 4
    t.integer "type_h",      limit: 4
    t.integer "type_m",      limit: 4
    t.integer "type_other",  limit: 4
    t.integer "var_a",       limit: 4
    t.integer "var_b",       limit: 4
    t.integer "var_c",       limit: 4
    t.integer "var_r",       limit: 4
    t.integer "var_d",       limit: 4
    t.integer "var_h",       limit: 4
    t.integer "var_n",       limit: 4
    t.integer "var_other",   limit: 4
    t.integer "known_a",     limit: 4
    t.integer "known_b",     limit: 4
    t.integer "known_c",     limit: 4
    t.integer "known_f",     limit: 4
    t.integer "known_k",     limit: 4
    t.integer "known_t",     limit: 4
    t.integer "known_v",     limit: 4
    t.integer "known_u",     limit: 4
    t.integer "known_p",     limit: 4
    t.integer "known_other", limit: 4
    t.integer "lang_ukr",    limit: 4
    t.integer "lang_rus",    limit: 4
    t.integer "lang_eng",    limit: 4
    t.integer "lang_ger",    limit: 4
    t.integer "lang_fre",    limit: 4
    t.integer "lang_other",  limit: 4
  end

  create_table "statistic_e", id: false, force: :cascade do |t|
    t.date    "dated",           null: false
    t.integer "vp_a",  limit: 4
    t.integer "vp_b",  limit: 4
    t.integer "vp_c",  limit: 4
    t.integer "vp_d",  limit: 4
    t.integer "vp_e",  limit: 4
    t.integer "vp_z",  limit: 4
    t.integer "ve_a",  limit: 4
    t.integer "ve_b",  limit: 4
    t.integer "ve_c",  limit: 4
    t.integer "ve_d",  limit: 4
    t.integer "ve_e",  limit: 4
    t.integer "ve_z",  limit: 4
    t.integer "np_a",  limit: 4
    t.integer "np_b",  limit: 4
    t.integer "np_c",  limit: 4
    t.integer "np_d",  limit: 4
    t.integer "np_e",  limit: 4
    t.integer "np_z",  limit: 4
    t.integer "ne_a",  limit: 4
    t.integer "ne_b",  limit: 4
    t.integer "ne_c",  limit: 4
    t.integer "ne_d",  limit: 4
    t.integer "ne_e",  limit: 4
    t.integer "ne_z",  limit: 4
    t.integer "zp_a",  limit: 4
    t.integer "zp_b",  limit: 4
    t.integer "zp_c",  limit: 4
    t.integer "zp_d",  limit: 4
    t.integer "zp_e",  limit: 4
    t.integer "zp_z",  limit: 4
    t.integer "ze_a",  limit: 4
    t.integer "ze_b",  limit: 4
    t.integer "ze_c",  limit: 4
    t.integer "ze_d",  limit: 4
    t.integer "ze_e",  limit: 4
    t.integer "ze_z",  limit: 4
  end

  create_table "statistic_h", id: false, force: :cascade do |t|
    t.date    "dated",                null: false
    t.integer "rh_11",      limit: 4
    t.integer "rv_11",      limit: 4
    t.integer "ru_11",      limit: 4
    t.integer "rv_a_11",    limit: 4
    t.integer "ru_a_11",    limit: 4
    t.integer "rv_b_11",    limit: 4
    t.integer "ru_b_11",    limit: 4
    t.integer "rv_c_11",    limit: 4
    t.integer "ru_c_11",    limit: 4
    t.integer "rv_d_11",    limit: 4
    t.integer "ru_d_11",    limit: 4
    t.integer "rv_e_11",    limit: 4
    t.integer "ru_e_11",    limit: 4
    t.integer "rh_12",      limit: 4
    t.integer "rv_12",      limit: 4
    t.integer "ru_12",      limit: 4
    t.integer "rv_a_12",    limit: 4
    t.integer "ru_a_12",    limit: 4
    t.integer "rv_b_12",    limit: 4
    t.integer "ru_b_12",    limit: 4
    t.integer "rv_c_12",    limit: 4
    t.integer "ru_c_12",    limit: 4
    t.integer "rv_d_12",    limit: 4
    t.integer "ru_d_12",    limit: 4
    t.integer "rv_e_12",    limit: 4
    t.integer "ru_e_12",    limit: 4
    t.integer "rh_29",      limit: 4
    t.integer "rv_29",      limit: 4
    t.integer "ru_29",      limit: 4
    t.integer "rv_a_29",    limit: 4
    t.integer "ru_a_29",    limit: 4
    t.integer "rv_b_29",    limit: 4
    t.integer "ru_b_29",    limit: 4
    t.integer "rv_c_29",    limit: 4
    t.integer "ru_c_29",    limit: 4
    t.integer "rv_d_29",    limit: 4
    t.integer "ru_d_29",    limit: 4
    t.integer "rv_e_29",    limit: 4
    t.integer "ru_e_29",    limit: 4
    t.integer "rh_54",      limit: 4
    t.integer "rv_54",      limit: 4
    t.integer "ru_54",      limit: 4
    t.integer "rv_a_54",    limit: 4
    t.integer "ru_a_54",    limit: 4
    t.integer "rv_b_54",    limit: 4
    t.integer "ru_b_54",    limit: 4
    t.integer "rv_c_54",    limit: 4
    t.integer "ru_c_54",    limit: 4
    t.integer "rv_d_54",    limit: 4
    t.integer "ru_d_54",    limit: 4
    t.integer "rv_e_54",    limit: 4
    t.integer "ru_e_54",    limit: 4
    t.integer "rh_55",      limit: 4
    t.integer "rv_55",      limit: 4
    t.integer "ru_55",      limit: 4
    t.integer "rv_a_55",    limit: 4
    t.integer "ru_a_55",    limit: 4
    t.integer "rv_b_55",    limit: 4
    t.integer "ru_b_55",    limit: 4
    t.integer "rv_c_55",    limit: 4
    t.integer "ru_c_55",    limit: 4
    t.integer "rv_d_55",    limit: 4
    t.integer "ru_d_55",    limit: 4
    t.integer "rv_e_55",    limit: 4
    t.integer "ru_e_55",    limit: 4
    t.integer "rh_133",     limit: 4
    t.integer "rv_133",     limit: 4
    t.integer "ru_133",     limit: 4
    t.integer "rv_a_133",   limit: 4
    t.integer "ru_a_133",   limit: 4
    t.integer "rv_b_133",   limit: 4
    t.integer "ru_b_133",   limit: 4
    t.integer "rv_c_133",   limit: 4
    t.integer "ru_c_133",   limit: 4
    t.integer "rv_d_133",   limit: 4
    t.integer "ru_d_133",   limit: 4
    t.integer "rv_e_133",   limit: 4
    t.integer "ru_e_133",   limit: 4
    t.integer "rh_99999",   limit: 4
    t.integer "rv_99999",   limit: 4
    t.integer "ru_99999",   limit: 4
    t.integer "rv_a_99999", limit: 4
    t.integer "ru_a_99999", limit: 4
    t.integer "rv_b_99999", limit: 4
    t.integer "ru_b_99999", limit: 4
    t.integer "rv_c_99999", limit: 4
    t.integer "ru_c_99999", limit: 4
    t.integer "rv_d_99999", limit: 4
    t.integer "ru_d_99999", limit: 4
    t.integer "rv_e_99999", limit: 4
    t.integer "ru_e_99999", limit: 4
    t.integer "rh_0",       limit: 4
    t.integer "rv_0",       limit: 4
    t.integer "ru_0",       limit: 4
    t.integer "rv_a_0",     limit: 4
    t.integer "ru_a_0",     limit: 4
    t.integer "rv_b_0",     limit: 4
    t.integer "ru_b_0",     limit: 4
    t.integer "rv_c_0",     limit: 4
    t.integer "ru_c_0",     limit: 4
    t.integer "rv_d_0",     limit: 4
    t.integer "ru_d_0",     limit: 4
    t.integer "rv_e_0",     limit: 4
    t.integer "ru_e_0",     limit: 4
  end

  create_table "statistic_m", id: false, force: :cascade do |t|
    t.date    "dated",             null: false
    t.integer "mnew",    limit: 4
    t.integer "mrer",    limit: 4
    t.integer "mreo",    limit: 4
    t.integer "vnew_7",  limit: 4
    t.integer "vrer_7",  limit: 4
    t.integer "vreo_7",  limit: 4
    t.integer "vnew_15", limit: 4
    t.integer "vrer_15", limit: 4
    t.integer "vreo_15", limit: 4
    t.integer "vnew_17", limit: 4
    t.integer "vrer_17", limit: 4
    t.integer "vreo_17", limit: 4
    t.integer "vnew_18", limit: 4
    t.integer "vrer_18", limit: 4
    t.integer "vreo_18", limit: 4
    t.integer "vnew_22", limit: 4
    t.integer "vrer_22", limit: 4
    t.integer "vreo_22", limit: 4
    t.integer "tnew_a",  limit: 4
    t.integer "trer_a",  limit: 4
    t.integer "treo_a",  limit: 4
    t.integer "tnew_b",  limit: 4
    t.integer "trer_b",  limit: 4
    t.integer "treo_b",  limit: 4
    t.integer "tnew_c",  limit: 4
    t.integer "trer_c",  limit: 4
    t.integer "treo_c",  limit: 4
    t.integer "tnew_d",  limit: 4
    t.integer "trer_d",  limit: 4
    t.integer "treo_d",  limit: 4
    t.integer "tnew_e",  limit: 4
    t.integer "trer_e",  limit: 4
    t.integer "treo_e",  limit: 4
  end

  create_table "stats_plan_12", primary_key: "recno_12", force: :cascade do |t|
    t.datetime "bdate"
    t.datetime "mdate"
    t.integer  "xyear",   limit: 4
    t.integer  "xquart",  limit: 2
    t.integer  "xtype",   limit: 4
    t.string   "subtype", limit: 10
    t.integer  "xplan",   limit: 4
    t.integer  "realy",   limit: 4
    t.integer  "xlevel",  limit: 2
    t.string   "sigla",   limit: 50
    t.date     "s_day"
  end

  add_index "stats_plan_12", ["xlevel"], name: "sp_12_xl"
  add_index "stats_plan_12", ["xtype"], name: "sp_12_xt"
  add_index "stats_plan_12", ["xyear"], name: "i_main_12"

  create_table "stock_45", primary_key: "recno_45", force: :cascade do |t|
    t.float    "invnum",   limit: 8
    t.integer  "recno23",  limit: 4
    t.string   "sigla",    limit: 50
    t.integer  "recno41",  limit: 4
    t.datetime "bdate"
    t.integer  "recno14",  limit: 4
    t.string   "comments", limit: 512
  end

  add_index "stock_45", ["recno14"], name: "fk_45_users_14"
  add_index "stock_45", ["recno23"], name: "fk_45_locati_23"
  add_index "stock_45", ["recno41"], name: "fk_45_org_41"
  add_index "stock_45", ["recno_45"], name: "pk_stock_45", unique: true

  create_table "string_26", primary_key: "recno_26", force: :cascade do |t|
    t.integer  "id",      limit: 4
    t.integer  "xindex",  limit: 4
    t.string   "txt",     limit: 90
    t.integer  "tag",     limit: 4,  default: 0
    t.string   "sysname", limit: 50
    t.datetime "bdate"
    t.datetime "mdate"
  end

  add_index "string_26", ["mdate", "id"], name: "main_26"

  create_table "struct_2", primary_key: "recno_2", force: :cascade do |t|
    t.string "stat_key",    limit: 36
    t.string "txt",         limit: 50
    t.text   "links"
    t.text   "fields"
    t.text   "deny_fields"
  end

  add_index "struct_2", ["stat_key"], name: "main_2"

  create_table "subj_category_34", primary_key: "recno_34", force: :cascade do |t|
    t.integer "recno2",        limit: 4,   null: false
    t.integer "id",            limit: 4
    t.string  "f_indicator",   limit: 2
    t.string  "start_data",    limit: 512
    t.string  "formal_title",  limit: 50
    t.string  "up_code_title", limit: 20
    t.string  "up_code",       limit: 20
    t.string  "up_title",      limit: 50
    t.string  "sys_code",      limit: 10
    t.integer "avt_num",       limit: 4
  end

  add_index "subj_category_34", ["id"], name: "ind_subj_category_34_id"
  add_index "subj_category_34", ["recno2"], name: "fk_subj_category_34_r2"

  create_table "subjects_13", primary_key: "recno_13", force: :cascade do |t|
    t.integer  "recno2",         limit: 4
    t.integer  "id",             limit: 4
    t.text     "address"
    t.string   "rest_data",      limit: 20
    t.integer  "add_data",       limit: 4
    t.string   "roman_nums",     limit: 10
    t.string   "inst_locat",     limit: 50
    t.string   "adv_rest_data",  limit: 50
    t.integer  "rest_bs_data",   limit: 4
    t.string   "dates",          limit: 20
    t.integer  "part_num",       limit: 4
    t.integer  "name_num",       limit: 4
    t.datetime "pub_date"
    t.string   "form_subtitle",  limit: 50
    t.string   "language",       limit: 20
    t.integer  "mix_info",       limit: 4
    t.string   "version",        limit: 20
    t.string   "start_data",     limit: 255
    t.integer  "formal_title",   limit: 4
    t.integer  "subj_subtitle",  limit: 4
    t.integer  "geo_subtitle",   limit: 4
    t.integer  "chron_subtitle", limit: 4
    t.string   "sys_code",       limit: 10
    t.integer  "avt_num",        limit: 4
    t.string   "organ_link",     limit: 90
    t.string   "invnum",         limit: 20
    t.string   "f_indicator",    limit: 2
  end

  add_index "subjects_13", ["id"], name: "ind_subjects_13_id"
  add_index "subjects_13", ["recno2"], name: "fk_subjects_13_r2"

  create_table "subscribe_100", primary_key: "recno_100", force: :cascade do |t|
    t.integer "recno102",    limit: 4
    t.integer "recno2",      limit: 4
    t.integer "recno84r",    limit: 4
    t.integer "set_count",   limit: 4
    t.integer "one_price",   limit: 4
    t.string  "s_plane",     limit: 20
    t.string  "subsc_index", limit: 10
    t.integer "set_price",   limit: 4
    t.integer "add_price",   limit: 4,  default: 0
  end

  add_index "subscribe_100", ["recno102"], name: "s100_r102"
  add_index "subscribe_100", ["recno2"], name: "s100_r2"
  add_index "subscribe_100", ["recno84r"], name: "s100_r84"

  create_table "subset_101", primary_key: "recno_101", force: :cascade do |t|
    t.integer "recno100",  limit: 4
    t.integer "set_count", limit: 4
    t.integer "recno41",   limit: 4
  end

  add_index "subset_101", ["recno100"], name: "s101_r100"
  add_index "subset_101", ["recno41"], name: "s101_r41"

  create_table "sum_book_112", primary_key: "recno_112", force: :cascade do |t|
    t.string   "status",       limit: 3, default: "n"
    t.datetime "bdate"
    t.integer  "id",           limit: 4
    t.datetime "idate"
    t.integer  "desc_count",   limit: 4
    t.integer  "book_count",   limit: 4
    t.integer  "balans_count", limit: 4
    t.integer  "balans_sum",   limit: 4
    t.integer  "out_count",    limit: 4
    t.integer  "recno15",      limit: 4
    t.integer  "type_b",       limit: 4
    t.integer  "type_h",       limit: 4
    t.integer  "type_other",   limit: 4
    t.integer  "var_a",        limit: 4
    t.integer  "var_c",        limit: 4
    t.integer  "var_d",        limit: 4
    t.integer  "var_other",    limit: 4
    t.integer  "known_a",      limit: 4
    t.integer  "known_c",      limit: 4
    t.integer  "known_b",      limit: 4
    t.integer  "known_other",  limit: 4
    t.integer  "lang_ukr",     limit: 4
    t.integer  "lang_rus",     limit: 4
    t.integer  "lang_other",   limit: 4
    t.integer  "type_e",       limit: 4
    t.integer  "type_f",       limit: 4
    t.integer  "type_m",       limit: 4
    t.integer  "var_b",        limit: 4
    t.integer  "var_r",        limit: 4
    t.integer  "var_h",        limit: 4
    t.integer  "var_n",        limit: 4
    t.integer  "known_f",      limit: 4
    t.integer  "known_k",      limit: 4
    t.integer  "known_t",      limit: 4
    t.integer  "known_v",      limit: 4
    t.integer  "known_u",      limit: 4
    t.integer  "known_p",      limit: 4
    t.integer  "lang_eng",     limit: 4
    t.integer  "lang_ger",     limit: 4
    t.integer  "lang_fre",     limit: 4
  end

  add_index "sum_book_112", ["id", "status"], name: "i_main_112"

  create_table "supplier_84", primary_key: "recno_84", force: :cascade do |t|
    t.string   "txt",                limit: 255
    t.string   "txt_a",              limit: 50
    t.string   "phone",              limit: 20
    t.string   "contact_people",     limit: 90
    t.text     "address"
    t.string   "email",              limit: 50
    t.string   "mfo",                limit: 20
    t.string   "xcode",              limit: 20
    t.string   "bank",               limit: 90
    t.text     "settlement_account"
    t.string   "status",             limit: 3,   default: "s"
    t.integer  "recno9",             limit: 4
    t.integer  "recno21",            limit: 4
    t.datetime "bdate"
    t.datetime "mdate"
  end

  add_index "supplier_84", ["recno21"], name: "s84_r21"
  add_index "supplier_84", ["recno9"], name: "s84_r9"

  create_table "systematizs_85", primary_key: "recno_85", force: :cascade do |t|
    t.string "txt",   limit: 255
    t.string "udk",   limit: 50
    t.string "subj",  limit: 50
    t.string "other", limit: 50
  end

  create_table "timedomain_82", primary_key: "recno_82", force: :cascade do |t|
    t.string  "txt",   limit: 50
    t.string  "data",  limit: 50
    t.integer "days",  limit: 2
    t.integer "hours", limit: 2
  end

  create_table "uddk_16", primary_key: "recno_16", force: :cascade do |t|
    t.integer "recno2",      limit: 4
    t.integer "id",          limit: 4
    t.string  "ud_index",    limit: 50
    t.string  "version",     limit: 20
    t.string  "ver_lang",    limit: 10
    t.string  "f_indicator", limit: 2
  end

  add_index "uddk_16", ["id"], name: "ind_uddk_16_id"
  add_index "uddk_16", ["recno2"], name: "fk_uddk_16_r2"

  create_table "unf_1", primary_key: "recno_1", force: :cascade do |t|
    t.integer "root",          limit: 4,   default: 0
    t.string  "txt_r",         limit: 128
    t.string  "txt_u",         limit: 128
    t.string  "repeatable",    limit: 1,   default: "0"
    t.string  "table_name",    limit: 20
    t.string  "field_name",    limit: 20
    t.integer "field_length",  limit: 4
    t.string  "rm_index",      limit: 10
    t.string  "rm_field",      limit: 1
    t.string  "rm_gid",        limit: 11
    t.string  "field_type",    limit: 20
    t.integer "field_num",     limit: 4
    t.string  "def_data",      limit: 255
    t.integer "vis",           limit: 4
    t.integer "virtual",       limit: 4
    t.string  "dstate",        limit: 10
    t.string  "f_indicator",   limit: 512
    t.integer "indicator_num", limit: 4
  end

  add_index "unf_1", ["root"], name: "u1_root"

  create_table "uni_title_30", primary_key: "recno_30", force: :cascade do |t|
    t.integer "recno2",         limit: 4
    t.string  "start_data",     limit: 50
    t.string  "sub_title",      limit: 90
    t.string  "day_mounth",     limit: 20
    t.string  "ref_start_data", limit: 50
    t.string  "self_name",      limit: 50
    t.string  "super_name",     limit: 50
    t.string  "name_num",       limit: 90
    t.string  "xyear",          limit: 10
    t.string  "ara_num",        limit: 10
    t.string  "poman_num",      limit: 10
    t.string  "location",       limit: 50
    t.string  "organ",          limit: 90
    t.string  "f_indicator",    limit: 2
  end

  add_index "uni_title_30", ["recno2"], name: "fk_uni_title_30_r2"

  create_table "uni_titles_29", primary_key: "recno_29", force: :cascade do |t|
    t.integer  "recno2",         limit: 4
    t.integer  "id",             limit: 4
    t.integer  "part_num",       limit: 4
    t.integer  "name_num",       limit: 4
    t.string   "form_subtitle",  limit: 50
    t.text     "mix_info"
    t.string   "version",        limit: 20
    t.string   "vol_mark",       limit: 20
    t.string   "start_data",     limit: 50
    t.integer  "rest_data",      limit: 4
    t.string   "ref_start_data", limit: 50
    t.integer  "formal_title",   limit: 4
    t.datetime "pub_date"
    t.string   "language",       limit: 20
    t.integer  "subj_subtitle",  limit: 4
    t.integer  "geo_subtitle",   limit: 4
    t.integer  "chron_subtitle", limit: 4
    t.string   "sys_code",       limit: 10
    t.integer  "avt_num",        limit: 4
    t.string   "f_indicator",    limit: 2
  end

  add_index "uni_titles_29", ["id"], name: "ind_uni_titles_29_id"
  add_index "uni_titles_29", ["recno2"], name: "fk_uni_titles_29_r2"

  create_table "unm_18", primary_key: "recno_18", force: :cascade do |t|
    t.string   "status",  limit: 10,   default: "n"
    t.integer  "recno14", limit: 4
    t.integer  "recno15", limit: 4
    t.datetime "bdate"
    t.datetime "rdate"
    t.datetime "edate"
    t.string   "request", limit: 4096
    t.string   "named",   limit: 90
  end

  add_index "unm_18", ["recno14"], name: "u18_r14"
  add_index "unm_18", ["recno15"], name: "u18_r15"

  create_table "unm_corporate_60", primary_key: "recno_60", force: :cascade do |t|
    t.integer "root",    limit: 4
    t.string  "title",   limit: 255
    t.integer "recno41", limit: 4
    t.text    "options"
  end

  add_index "unm_corporate_60", ["recno41"], name: "uc60_r41"
  add_index "unm_corporate_60", ["root"], name: "uc60_root"

  create_table "unm_corporate_credits_63", primary_key: "recno_63", force: :cascade do |t|
    t.integer  "recno62",     limit: 4
    t.integer  "recno41",     limit: 4
    t.string   "status",      limit: 10, default: "n"
    t.integer  "u_object",    limit: 4
    t.integer  "u_subject",   limit: 4
    t.integer  "u_credit",    limit: 4
    t.integer  "u_duty",      limit: 4
    t.integer  "u_right",     limit: 4
    t.datetime "date_start"
    t.integer  "time_period", limit: 4
    t.integer  "time_pause",  limit: 4
    t.integer  "time_work",   limit: 4
  end

  add_index "unm_corporate_credits_63", ["recno41"], name: "uc63_r41"
  add_index "unm_corporate_credits_63", ["recno62"], name: "uc63_r62"

  create_table "unm_corporate_options_62", primary_key: "recno_62", force: :cascade do |t|
    t.integer  "recno41",  limit: 4
    t.integer  "recno41r", limit: 4
    t.string   "status",   limit: 10, default: "n"
    t.string   "options",  limit: 90
    t.datetime "bdate"
    t.datetime "edate"
    t.datetime "mdate"
  end

  add_index "unm_corporate_options_62", ["recno41"], name: "uo62_r41"
  add_index "unm_corporate_options_62", ["recno41r"], name: "uo62_r41r"

  create_table "unm_requests_19", primary_key: "recno_19", force: :cascade do |t|
    t.integer  "recno18",   limit: 4
    t.integer  "recno41",   limit: 4
    t.string   "status",    limit: 10, default: "n"
    t.datetime "edate"
    t.binary   "outcome"
    t.integer  "rec_count", limit: 4
    t.integer  "rec_left",  limit: 4
  end

  add_index "unm_requests_19", ["recno18"], name: "ur19_r18"
  add_index "unm_requests_19", ["recno41"], name: "ur19_r41"

  create_table "unm_results_20", primary_key: "recno_20", force: :cascade do |t|
    t.integer "recno19", limit: 4, null: false
    t.integer "recno",   limit: 4
    t.text    "data"
  end

  add_index "unm_results_20", ["recno"], name: "unm_results_20_idx1"
  add_index "unm_results_20", ["recno19"], name: "ur20_r19"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: ""
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "name",                   limit: 255
    t.integer  "recno_15",               limit: 4
    t.integer  "role",                   limit: 4,   default: 0
  end

  add_index "users", ["reset_password_token"], name: "users_reset_password_token", unique: true

  create_table "users_14", primary_key: "recno_14", force: :cascade do |t|
    t.string   "nick",    limit: 20
    t.string   "name",    limit: 128
    t.string   "pass",    limit: 50
    t.integer  "recno41", limit: 4,   default: 0
    t.string   "rights",  limit: 128
    t.integer  "linked",  limit: 4
    t.string   "linkid",  limit: 1
    t.datetime "bdate"
    t.datetime "mdate"
  end

  add_index "users_14", ["recno41"], name: "u14_r41"

  create_table "viewslib_3", primary_key: "recno_3", force: :cascade do |t|
    t.string  "txt",          limit: 90
    t.string  "vis",          limit: 1
    t.integer "icon",         limit: 4
    t.string  "marker",       limit: 36
    t.text    "fields"
    t.text    "short_fields"
  end

  create_table "volumes_11", primary_key: "recno_11", force: :cascade do |t|
    t.integer "recno2",        limit: 4
    t.string  "capacity",      limit: 50
    t.string  "illustrations", limit: 20
    t.string  "sizes",         limit: 50
    t.text    "accomp_mat"
    t.string  "f_indicator",   limit: 2
  end

  add_index "volumes_11", ["recno2"], name: "fk_volumes_11_r2"

  create_table "waybill_90", primary_key: "recno_90", force: :cascade do |t|
    t.integer  "root",          limit: 4
    t.string   "status",        limit: 3,  default: "nn"
    t.datetime "bdate"
    t.integer  "id",            limit: 4
    t.integer  "air",           limit: 4
    t.datetime "idate"
    t.integer  "will_provider", limit: 4
    t.string   "will_id",       limit: 20
    t.datetime "will_date"
    t.integer  "balans_count",  limit: 4
    t.integer  "balans_sum",    limit: 4
    t.integer  "out_count",     limit: 4
    t.integer  "desc_count",    limit: 4
    t.integer  "book_count",    limit: 4
    t.integer  "recno14",       limit: 4
    t.integer  "recno41",       limit: 4
    t.integer  "type_b",        limit: 4
    t.integer  "type_h",        limit: 4
    t.integer  "type_other",    limit: 4
    t.integer  "var_a",         limit: 4
    t.integer  "var_c",         limit: 4
    t.integer  "var_d",         limit: 4
    t.integer  "var_other",     limit: 4
    t.integer  "known_a",       limit: 4
    t.integer  "known_c",       limit: 4
    t.integer  "known_other",   limit: 4
    t.integer  "lang_ukr",      limit: 4
    t.integer  "lang_rus",      limit: 4
    t.integer  "lang_other",    limit: 4
    t.integer  "known_b",       limit: 4
    t.integer  "type_e",        limit: 4
    t.integer  "type_f",        limit: 4
    t.integer  "type_m",        limit: 4
    t.integer  "var_b",         limit: 4
    t.integer  "var_r",         limit: 4
    t.integer  "var_h",         limit: 4
    t.integer  "var_n",         limit: 4
    t.integer  "known_f",       limit: 4
    t.integer  "known_k",       limit: 4
    t.integer  "known_t",       limit: 4
    t.integer  "known_v",       limit: 4
    t.integer  "known_u",       limit: 4
    t.integer  "known_p",       limit: 4
    t.integer  "lang_eng",      limit: 4
    t.integer  "lang_ger",      limit: 4
    t.integer  "lang_fre",      limit: 4
  end

  add_index "waybill_90", ["recno14"], name: "w90_r14"
  add_index "waybill_90", ["recno41"], name: "w90_r41"
  add_index "waybill_90", ["root"], name: "w90_root"

  create_table "wo_detail_111", primary_key: "recno_111", force: :cascade do |t|
    t.integer  "recno110", limit: 4
    t.datetime "bdate"
    t.integer  "recno23",  limit: 4
    t.integer  "uid",      limit: 4
    t.integer  "cause",    limit: 4
    t.string   "sigla",    limit: 50
    t.integer  "recno84",  limit: 4
    t.integer  "recno15",  limit: 4
    t.integer  "price",    limit: 4
  end

  add_index "wo_detail_111", ["recno110"], name: "wd111_r110"
  add_index "wo_detail_111", ["recno15"], name: "wd111_r15"
  add_index "wo_detail_111", ["recno23"], name: "wd111_r23"
  add_index "wo_detail_111", ["recno23"], name: "wod111_r23"
  add_index "wo_detail_111", ["recno84"], name: "wd111_r84"

  create_table "workaria_48", primary_key: "recno_48", force: :cascade do |t|
    t.integer  "recno14", limit: 4
    t.integer  "recno2",  limit: 4
    t.datetime "bdate"
  end

  add_index "workaria_48", ["recno14"], name: "w48_r14"
  add_index "workaria_48", ["recno2"], name: "w48_r2"

  create_table "write_out_110", primary_key: "recno_110", force: :cascade do |t|
    t.integer  "root",         limit: 4
    t.string   "status",       limit: 3
    t.integer  "wo_cause",     limit: 4
    t.datetime "bdate"
    t.integer  "id",           limit: 4
    t.datetime "idate"
    t.datetime "wdate"
    t.integer  "desc_count",   limit: 4
    t.integer  "book_count",   limit: 4
    t.integer  "balans_count", limit: 4
    t.integer  "balans_sum",   limit: 4
    t.integer  "out_count",    limit: 4
    t.integer  "recno14",      limit: 4
    t.integer  "recno41",      limit: 4
    t.integer  "type_b",       limit: 4
    t.integer  "type_h",       limit: 4
    t.integer  "type_other",   limit: 4
    t.integer  "var_a",        limit: 4
    t.integer  "var_c",        limit: 4
    t.integer  "var_d",        limit: 4
    t.integer  "var_other",    limit: 4
    t.integer  "known_a",      limit: 4
    t.integer  "known_c",      limit: 4
    t.integer  "known_b",      limit: 4
    t.integer  "known_other",  limit: 4
    t.integer  "lang_ukr",     limit: 4
    t.integer  "lang_rus",     limit: 4
    t.integer  "lang_other",   limit: 4
    t.integer  "type_e",       limit: 4
    t.integer  "type_f",       limit: 4
    t.integer  "type_m",       limit: 4
    t.integer  "var_b",        limit: 4
    t.integer  "var_r",        limit: 4
    t.integer  "var_h",        limit: 4
    t.integer  "var_n",        limit: 4
    t.integer  "known_f",      limit: 4
    t.integer  "known_k",      limit: 4
    t.integer  "known_t",      limit: 4
    t.integer  "known_v",      limit: 4
    t.integer  "known_u",      limit: 4
    t.integer  "known_p",      limit: 4
    t.integer  "lang_eng",     limit: 4
    t.integer  "lang_ger",     limit: 4
    t.integer  "lang_fre",     limit: 4
  end

  add_index "write_out_110", ["recno14"], name: "wo110_r14"
  add_index "write_out_110", ["recno41"], name: "wo110_r41"
  add_index "write_out_110", ["root"], name: "wo110_root"

  create_table "xtree_21", primary_key: "recno_21", force: :cascade do |t|
    t.string  "id",    limit: 1
    t.integer "root",  limit: 4
    t.integer "ind",   limit: 4
    t.string  "txt",   limit: 255
    t.string  "txta",  limit: 50
    t.integer "tag",   limit: 4
    t.integer "tag_1", limit: 4
    t.string  "subid", limit: 90
  end

  add_index "xtree_21", ["root"], name: "x21_root"

end
